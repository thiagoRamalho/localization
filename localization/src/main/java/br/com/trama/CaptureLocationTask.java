package br.com.trama;

import java.util.Calendar;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.util.Log;

public class CaptureLocationTask{

	private final String TAG = CaptureLocationTask.class.getSimpleName();

	//conversao para minutos
	private static final long MIN_TIME = 10000;
	//em metros
	private static final float MIN_DISTANCE = 5;

	private Context context;
	private LocationManager locationManager;
	private String bestProvider;
	private DataBaseHelper helper;

	private LocationListener listener;

	public CaptureLocationTask(Context context) {
		this.context = context;
		this.helper = new DataBaseHelper(context);
		Log.d(TAG, "constructor");
	}


	public boolean run(){
		Log.d(TAG, "run");
		return this.getProvider();
	}

	private boolean getProvider() {

		Log.d(TAG, "getProvider - ini");


		boolean isOk = false;

		if(locationManager == null){
			locationManager = (LocationManager) this.context.getSystemService(Context.LOCATION_SERVICE);
		}

		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setPowerRequirement(Criteria.POWER_LOW); 

		bestProvider = locationManager.getBestProvider(criteria, true);

		if((bestProvider != null) && (!"".equals(bestProvider.trim()))){

			Log.d(TAG, "getProvider - bestProvider: "+bestProvider);

			if(this.listener == null){
				this.listener = new LocListener();
			}
			
			this.locationManager.removeUpdates(this.listener);
			
			 Intent intent = new Intent(this.context.getApplicationContext(), LocationReceiver.class);
			  PendingIntent locationIntent = PendingIntent.getBroadcast(this.context.getApplicationContext(), 14872, intent, PendingIntent.FLAG_CANCEL_CURRENT);
			
			this.locationManager.requestLocationUpdates(MIN_TIME, MIN_DISTANCE, criteria, locationIntent);
			
			this.locationManager.getLastKnownLocation(bestProvider);

			isOk = true;
		}

		Log.d(TAG, "getProvider - fim");

		return isOk;
	}

	class LocListener implements LocationListener{

		public void onLocationChanged(Location l) {
			
			LocationEntity locationEntity = new LocationEntity(l.getLatitude(), l.getLongitude(), bestProvider, Calendar.getInstance());
		
			helper.save(locationEntity);

			Log.d(this.getClass().getSimpleName(), "onLocationChanged "+locationEntity.toString());
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
			
			String statusDescription = status == LocationProvider.AVAILABLE ? "AVALIABLE" : status == LocationProvider.OUT_OF_SERVICE ? "OUT_OF_SERVICE" : "TEMPORARILY_UNAVAILABLE"; 
			
			Log.d(this.getClass().getSimpleName(), "onStatusChanged: provider["+provider+"] status ["+statusDescription+"]");
		}

		public void onProviderEnabled(String provider) {
			run();		
		}

		public void onProviderDisabled(String provider) {
			run();		
		}
	}
}
