package br.com.trama;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper{

	public static final int DATABASE_VERSION = 2;
	public static final String DATABASE_NAME = "location.db";
	

	public DataBaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(getCreate());
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	       db.execSQL(getDrop());
	       onCreate(db);
	}

	
	private String getDrop() {
		return  "DROP TABLE IF EXISTS LOCATION";
	}

	private String getCreate() {

		StringBuilder builder = new StringBuilder();

		builder
		.append(" CREATE ")
		.append(" TABLE ")
		.append(" IF NOT EXISTS ")
		.append(" LOCATION ")
		.append(" ( ")
		.append(" ID ")
		.append(" INTEGER PRIMARY KEY AUTOINCREMENT, ")
		.append(" LAT ")
		.append(" REAL NOT NULL, ")
		.append(" LON ")
		.append(" REAL NOT NULL, ")
		.append(" DATE ")
		.append(" TEXT NOT NULL, ")		
		.append(" PROVIDER ")
		.append(" TEXT NOT NULL ")
		.append(" ) ");

		return builder.toString();
	}
	
	public void save(LocationEntity entity){
		
		SQLiteDatabase db = this.getWritableDatabase();
		
		db.insert("LOCATION", "", getValues(entity));
	}

	private ContentValues getValues(LocationEntity entity) {
		
		ContentValues values = new ContentValues();
		values.put("LAT",      entity.getLatitude());
		values.put("LON",      entity.getLongitude());
		values.put("PROVIDER", entity.getProvider());
		values.put("DATE",     entity.getDate().getTimeInMillis());
		
		return values;
	}
	
	public List<LocationEntity> selectAll(){

		ArrayList<LocationEntity> arrayList = new ArrayList<LocationEntity>();

		SQLiteDatabase db = this.getWritableDatabase();

		Cursor cursor = db.rawQuery(" SELECT * FROM LOCATION ", new String[0]);

		while(cursor.moveToNext()){

			double latitude  = cursor.getDouble(cursor.getColumnIndex("LAT"));
			double longitude = cursor.getDouble(cursor.getColumnIndex("LON"));
			String provider  = cursor.getString(cursor.getColumnIndex("PROVIDER"));
			long dateInMillis= cursor.getLong  (cursor.getColumnIndex("DATE"));

			Calendar date = Calendar.getInstance();
			date.setTimeInMillis(dateInMillis);
			arrayList.add(new LocationEntity(latitude, longitude, provider, date));
		}

		cursor.close();

		return arrayList;
	}
}
