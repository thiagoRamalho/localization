package br.com.trama;

import java.util.Calendar;

public class LocationEntity {

	private double latitude;
	private double longitude;
	private String provider;
	
	private Calendar date;
	public LocationEntity(double latitude, double longitude, String provider,
			Calendar date) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.provider = provider;
		this.date = date;
	}
	
	public double getLatitude() {
		return latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public String getProvider() {
		return provider;
	}
	public Calendar getDate() {
		return date;
	}

	@Override
	public String toString() {
		return "LocationEntity [latitude=" + latitude + ", longitude="
				+ longitude + ", provider=" + provider + "]";
	}
	
	@Override
	public boolean equals(Object o) {
		if(o == null){
			return true;
		}
		else {
			return this.toString().equals(o.toString());
		}
	}
}
