package br.com.trama;

import java.util.Calendar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

public class LocationReceiver extends BroadcastReceiver {

		private static final String TAG = LocationReceiver.class.getSimpleName();
		
		@Override
		public void onReceive(Context context, Intent intent) {

			Location location = (Location) intent.getExtras().get(LocationManager.KEY_LOCATION_CHANGED);

			Log.d(TAG, "LocationReceiver");

			if(location != null){
				LocationEntity locationEntity = new LocationEntity(location.getLatitude(), location.getLongitude(), location.getProvider(), Calendar.getInstance());
				
				Log.d(TAG, "LocationReceiver "+locationEntity.toString());
				
				new DataBaseHelper(context).save(locationEntity);
			}
		}
	}