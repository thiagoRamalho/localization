package br.com.trama;

import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {

	private Handler customHandler;
	private DataBaseHelper dataBaseHelper;
	private LinearLayout linearLayout;
	private TextView sizeTxt;
	private CaptureLocationTask captureLocationTask;
	protected Object last;

	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		linearLayout = (LinearLayout)findViewById(R.id.LinearLayout1);
		
		sizeTxt	= (TextView)findViewById(R.id.txtSize);
		
		dataBaseHelper = new DataBaseHelper(getApplicationContext());
		
		customHandler = new android.os.Handler();
		customHandler.postDelayed(updateTimerThread, 0);
	}

	@Override
	protected void onResume() {

		super.onResume();

		if(captureLocationTask == null){
			captureLocationTask = new CaptureLocationTask(getApplicationContext());
			captureLocationTask.run();
		}
	}
	
	private Runnable updateTimerThread = new Runnable(){
		public void run(){
			
			List<LocationEntity> selectAll = dataBaseHelper.selectAll();
			
			if(!selectAll.isEmpty()){
				
				sizeTxt.setText(selectAll.size()+"");
				
				LocationEntity atual = selectAll.get(selectAll.size()-1);
				
					TextView t2 = new TextView(getApplicationContext());
					t2.setText(atual.toString());
					t2.setTextColor(Color.BLACK);
					linearLayout.addView(t2);
			}
			
			customHandler.postDelayed(this, 10000);
		}
	};

}